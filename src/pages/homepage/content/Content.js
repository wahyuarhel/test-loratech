import React from 'react'
import './Content.scss'

export default function Content() {
    return (
        <div className="content_container">
            <div className="card_container">
                <div>
                    <ul>
                        <li>
                            <div className="blue_card">
                                <div >
                                    <img src="https://static.toss.im/icons/svg/icn-emoji-loudly-crying-face.svg" alt="sad" />
                                    <label>6,339번째 불편함</label>
                                </div>
                                <p>거래중지된 계좌에서 잔액이 남아있는데 중지되서 받을수도없구 송금버튼도없음</p>
                            </div>
                        </li>
                        <li>
                            <div className="green_card">
                                <div >
                                    <img src="https://static.toss.im/icons/svg/icn-emoji-face-with-steam-from-nose.svg" alt="angry" />
                                    <label>5,708</label>
                                </div>
                                <p>보유한 주식 채권 등과 예적금 등을 한곳에 모아보고 싶은데 다 분리되어 있으니 어플 왔다갔다 불편해요</p>
                            </div>
                        </li>
                        <li>
                            <div className="grey_card">
                                <div >
                                    <img src="https://static.toss.im/icons/svg/icn-emoji-pile-of-poo.svg" alt="shit" />
                                    <label>4,884번째 불편함</label>
                                </div>
                                <p>자동이체를 깜박 까먹었을때 미리 알림 서비스가 있었으면</p>
                            </div>
                        </li>
                        <li>
                            <div className="brown_card">
                                <div >
                                    <img src="https://static.toss.im/icons/svg/icn-emoji-skull-and-crossbones.svg" alt="skeleton" />
                                    <label>6,339번째 불편함</label>
                                </div>
                                <p>거래중지된 계좌에서 잔액이 남아있는데 중지되서 받을수도없구 송금버튼도없음</p>
                            </div>
                        </li>
                    </ul>
                </div>

                <div>
                    <ul>
                        <li>
                            <div className="blue_card">
                                <div >
                                    <img src="https://static.toss.im/icons/svg/icn-emoji-loudly-crying-face.svg" alt="sad" />
                                    <label>6,339번째 불편함</label>
                                </div>
                                <p>거래중지된 계좌에서 잔액이 남아있는데 중지되서 받을수도없구 송금버튼도없음</p>
                            </div>
                        </li>
                        <li>
                            <div className="green_card">
                                <div >
                                    <img src="https://static.toss.im/icons/svg/icn-emoji-face-with-steam-from-nose.svg" alt="angry" />
                                    <label>5,708</label>
                                </div>
                                <p>보유한 주식 채권 등과 예적금 등을 한곳에 모아보고 싶은데 다 분리되어 있으니 어플 왔다갔다 불편해요</p>
                            </div>
                        </li>
                        <li>
                            <div className="grey_card">
                                <div >
                                    <img src="https://static.toss.im/icons/svg/icn-emoji-pile-of-poo.svg" alt="shit" />
                                    <label>4,884번째 불편함</label>
                                </div>
                                <p>자동이체를 깜박 까먹었을때 미리 알림 서비스가 있었으면</p>
                            </div>
                        </li>
                        <li>
                            <div className="brown_card">
                                <div >
                                    <img src="https://static.toss.im/icons/svg/icn-emoji-skull-and-crossbones.svg" alt="skeleton" />
                                    <label>6,339번째 불편함</label>
                                </div>
                                <p>거래중지된 계좌에서 잔액이 남아있는데 중지되서 받을수도없구 송금버튼도없음</p>
                            </div>
                        </li>
                    </ul>
                </div>

                <div>
                    <ul>
                        <li>
                            <div className="blue_card">
                                <div >
                                    <img src="https://static.toss.im/icons/svg/icn-emoji-loudly-crying-face.svg" alt="sad" />
                                    <label>6,339번째 불편함</label>
                                </div>
                                <p>거래중지된 계좌에서 잔액이 남아있는데 중지되서 받을수도없구 송금버튼도없음</p>
                            </div>
                        </li>
                        <li>
                            <div className="green_card">
                                <div >
                                    <img src="https://static.toss.im/icons/svg/icn-emoji-face-with-steam-from-nose.svg" alt="angry" />
                                    <label>5,708</label>
                                </div>
                                <p>보유한 주식 채권 등과 예적금 등을 한곳에 모아보고 싶은데 다 분리되어 있으니 어플 왔다갔다 불편해요</p>
                            </div>
                        </li>
                        <li>
                            <div className="grey_card">
                                <div >
                                    <img src="https://static.toss.im/icons/svg/icn-emoji-pile-of-poo.svg" alt="shit" />
                                    <label>4,884번째 불편함</label>
                                </div>
                                <p>자동이체를 깜박 까먹었을때 미리 알림 서비스가 있었으면</p>
                            </div>
                        </li>
                        <li>
                            <div className="brown_card">
                                <div >
                                    <img src="https://static.toss.im/icons/svg/icn-emoji-skull-and-crossbones.svg" alt="skeleton" />
                                    <label>6,339번째 불편함</label>
                                </div>
                                <p>거래중지된 계좌에서 잔액이 남아있는데 중지되서 받을수도없구 송금버튼도없음</p>
                            </div>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    )
}
